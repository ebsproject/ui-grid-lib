import React from "react";
import PropTypes from "prop-types";
import Client from "./Utils/Client";
import EbsGrid from "./components/Organisms/MainTable";
import { extractColumns } from "./Utils/Client";
import { buildQuery } from "./functions/query";
import AlertNotification from "./components/Atoms/AlertNotification";
import "regenerator-runtime/runtime";

// @param uri: endpoint Uri
// @param title: Table title
// @param select: Component or kind of select implemented
// @param entity: entity to handle
// @param height: table height
// @param indexing: table indexing
// @param woractions: Component to add on every row as Actions
// @param callstandar: Brapi/Graphql standards to handle
// @param toolbaractions: Component to add on top table as Global actions
// @param detailcomponent: Component used as master detail on every row
const EbsGridLib = React.forwardRef(
  (
    {
      columns: originalColumns,
      uri,
      title,
      select,
      entity,
      height,
      indexing,
      rowactions,
      callstandard,
      toolbaractions,
      detailcomponent,
    },
    ref
  ) => {
    const [alertState, setAlertState] = React.useState({
      open: false,
      message: "",
    });
    const columns = React.useMemo(() => originalColumns, []);
    // * We'll start our table without any data
    const [data, setData] = React.useState([]);
    const [loading, setLoading] = React.useState(false);
    const [pageCount, setPageCount] = React.useState(0);
    const fetchIdRef = React.useRef(0);
    const extractedColumns = extractColumns(originalColumns);
    // * Close notification fuction
    const handleClose = (event, reason) => {
      if (reason === "clickaway") {
        return;
      }
      setAlertState({
        open: false,
        message: "",
      });
    };
    // * Fetch our data function
    const fetchData = React.useCallback(
      ({ pageSize, pageIndex, sortBy, filters, globalFilter }) => {
        let Filters = [];
        const page = { number: pageIndex, size: pageSize };
        const sort = [];
        // TODO: Set up Sorting
        sortBy.map((column) => {
          sort.push({ col: column.id, mod: column.desc ? "DES" : "ASC" });
        });
        // * This will get called when the table needs new data
        // * Give this fetch an ID
        const fetchId = ++fetchIdRef.current;

        // * Set the loading state
        setLoading(true);

        // * Setting up filters
        filters.map((filter) =>
          Filters.push({ col: filter.id, mod: "LK", val: filter.value })
        );
        // ? Global filter ?
        globalFilter &&
          extractedColumns.map((filter) => {
            !filter.hidden &&
              Filters.push({
                col: filter.accessor,
                mod: "LK",
                val: globalFilter,
              });
          });
        // * Only update the data if this is the latest fetch
        // TODO: Fix multi sorting in the API
        if (fetchId === fetchIdRef.current) {
          Client({
            uri: uri,
            page: page,
            sort: null, // TODO: enable sorting
            entity: entity,
            filters: Filters,
            columns: extractedColumns,
            callStandard: callstandard,
          })
            .then((response) => {
              setPageCount(response.pages);
              setLoading(false);
              setData(response.data);
            })
            .catch(({ message }) => {
              setAlertState({
                open: true,
                message: `${title}: ${message}`,
              });
            });
        }
      },
      []
    );

    return (
      <React.Fragment>
        <AlertNotification {...alertState} handleClose={handleClose} />
        <EbsGrid
          data={data}
          title={title}
          height={height}
          select={select}
          columns={columns}
          loading={loading}
          indexing={indexing}
          fetchData={fetchData}
          pageCount={pageCount}
          rowActions={rowactions}
          toolbarActions={toolbaractions}
          renderRowSubComponent={detailcomponent}
        />
      </React.Fragment>
    );
  }
);

EbsGrid.propTypes = {
  uri: PropTypes.string,
  title: PropTypes.node,
  select: PropTypes.any,
  entity: PropTypes.string,
  height: PropTypes.any.isRequired,
  columns: PropTypes.array.isRequired,
  indexing: PropTypes.bool,
  rowactions: PropTypes.func,
  callstandard: PropTypes.string,
  toolbaractions: PropTypes.func,
  detailcomponent: PropTypes.func,
};

export default EbsGridLib;
