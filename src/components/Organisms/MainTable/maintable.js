import React from "react";
import PropTypes from "prop-types";
import {
  useTable,
  useSortBy,
  useGroupBy,
  useFilters,
  useExpanded,
  usePagination,
  useBlockLayout,
  useGlobalFilter,
  useResizeColumns,
  useRowSelect,
} from "react-table";
import { useSticky } from "react-table-sticky";
import { Box, Divider, Typography, IconButton } from "@material-ui/core";
// * Icons
import {
  Lens,
  ExpandLess,
  ExpandMore,
  ArrowUpward,
  SortByAlpha,
  ArrowDownward,
  PanoramaFishEye,
  KeyboardArrowUp,
  KeyboardArrowDown,
} from "@material-ui/icons";
import Select from "../../Atoms/Select";
import Progress from "../../Atoms/Progress";
import Toolbar from "../../Molecules/Toolbar";
import Pagination from "../../Atoms/Pagination";
import DefaultFilter from "../../Atoms/DefaultFilter";
import { Styles } from "./styles";
// * MAIN FUNCTION

/*
 @prop columns columns to build headers
 @prop data data to display
 @prop tilte Table title
 @prop loading Loading flag
 @prop pageCount: controlledPageCount controlled pagination
 @prop height Height table
 @prop indexing Indexing flag to build indexing column
 @prop rowActions Component to display on every row as actions
 @prop toolbarActions Component to display on top table as global actions
 @prop select Select flag and kind of select to apply (it can be a custom component setup by develop)
 @prop renderRowSubComponent Component to display on every row as Master detail component
 @param ref reference made by React.forward
*/
const MainTableOrganism = React.forwardRef(
  (
    {
      columns,
      data,
      fetchData,
      title,
      loading,
      pageCount: controlledPageCount,
      height,
      indexing,
      rowActions,
      toolbarActions,
      select,
      renderRowSubComponent,
    },
    ref
  ) => {
    const [refresh, setRefresh] = React.useState(0);
    const refreshTable = () => setRefresh(refresh + 1);
    // * Setting up default column filters
    const defaultColumn = React.useMemo(
      () => ({
        // * Let's set up our default Filter UI
        Filter: DefaultFilter,
      }),
      []
    );

    // * Defining hidden columns
    const hiddenColumns = [];
    columns.map((header) => {
      if (header.columns) {
        header.columns.map((column) => {
          column.hidden
            ? hiddenColumns.push(column.accessor)
            : hiddenColumns.push(column.id);
        });
      } else {
        header.hidden
          ? hiddenColumns.push(header.accessor)
          : hiddenColumns.push(header.id);
      }
    });
    // ? row Actions ?
    !rowActions && hiddenColumns.push("rowActions");
    // ? indexing ?
    !indexing && hiddenColumns.push("indexing");
    // ? Selectable ?
    !select && hiddenColumns.push("selection");
    // ? render row sub component ?
    !renderRowSubComponent && hiddenColumns.push("detail");
    // * Setting up React-Table Hooks
    const {
      getTableProps,
      getTableBodyProps,
      headerGroups,
      prepareRow,
      rows,
      page,
      pageOptions,
      gotoPage,
      setPageSize,
      visibleColumns,
      selectedFlatRows,
      allColumns,
      getToggleHideAllColumnsProps,
      // * Get the state from the instance
      state: { pageIndex, pageSize, sortBy, filters, globalFilter },
      setGlobalFilter,
    } = useTable(
      {
        columns,
        data,
        initialState: {
          pageSize: 10,
          pageIndex: 0,
          // * Defining hidden columns by default
          hiddenColumns: hiddenColumns,
        },
        // * Pass our hoisted table state
        manualPagination: true, // * Tell the usePagination
        manualSortBy: true,
        autoResetPage: false,
        autoResetSortBy: false,
        manualFilters: true,
        manualGlobalFilter: true,
        // * hook that we'll handle our own data fetching
        // * This means we'll also have to provide our own
        // * pageCount.
        pageCount: controlledPageCount,
        defaultColumn,
      },
      useFilters,
      useGlobalFilter,
      useGroupBy,
      useSortBy,
      useExpanded,
      usePagination,
      useBlockLayout,
      useResizeColumns,
      useRowSelect,
      useSticky,
      (hooks) => {
        hooks.visibleColumns.push((columns) => [
          // * Indexing column
          {
            id: "indexing",
            Cell: ({ row }) => (
              <Typography variant="button">{row.index + 1}</Typography>
            ),
            width: 35,
            sticky: "left",
            disableResizing: true,
          },
          {
            id: "selection",
            // * The header can use the table's getToggleAllRowsSelectedProps method
            // * to render a checkbox
            Header: ({ getToggleAllRowsSelectedProps }) => {
              if (select) {
                switch (select) {
                  case "multi":
                    return <Select {...getToggleAllRowsSelectedProps()} />;
                  default:
                    return <div />;
                }
              } else {
                return <div />;
              }
            },
            // * The cell can use the individual row's getToggleRowSelectedProps method
            // * to the render a checkbox
            Cell: ({ row }) => {
              if (select) {
                switch (select) {
                  case "multi":
                    return <Select {...row.getToggleRowSelectedProps()} />;
                  case "single":
                    if (
                      rows.filter((row) => row.isSelected).length < 1 ||
                      row.isSelected
                    ) {
                      return <Select {...row.getToggleRowSelectedProps()} />;
                    } else {
                      return (
                        <Select
                          checked={false}
                          readOnly
                          style={row.getToggleRowSelectedProps().style}
                        />
                      );
                    }
                  default:
                    // * Fucntion to handle a custom select
                    return select({ rows, row });
                }
              } else {
                return String("");
              }
            },
            width: 55,
            sticky: "left",
            disableResizing: true,
            hidden: select ? false : true,
          },
          {
            // * Build our expander column to display detail component
            id: "details", // * Make sure it has an ID
            Cell: ({ row }) => {
              // * Use Cell to render an expander for each row.
              // * We can use the getToggleRowExpandedProps prop-getter
              // * to build the expander.
              return (
                <span {...row.getToggleRowExpandedProps()}>
                  <IconButton size="small">
                    {row.isExpanded ? (
                      <KeyboardArrowUp />
                    ) : (
                      <KeyboardArrowDown />
                    )}
                  </IconButton>
                </span>
              );
            },
            width: 40,
            sticky: "left",
            disableResizing: true,
            hidden: renderRowSubComponent ? false : true,
          },
          {
            id: "rowActions",
            Cell: ({ row }) => {
              return (rowActions && rowActions(row, refresh)) || String("");
            },
            width:
              55 *
              ((rowActions && rowActions({}, () => {}).props.children.length) ||
                1),
            sticky: "left",
            disableResizing: true,
            hidden: rowActions ? false : true,
          },
          ...columns,
        ]);
      }
    );

    // * Listen for changes in pagination, sorting or filtering and use the state to fetch our new data
    React.useEffect(() => {
      fetchData({ pageIndex, pageSize, sortBy, filters, globalFilter });
    }, [
      sortBy,
      fetchData,
      pageIndex,
      pageSize,
      filters,
      globalFilter,
      refresh,
    ]);

    // * Page changes
    const handleChange = (e, page) => {
      gotoPage(page - 1);
    };

    return (
      /* 
     @prop data-testid: Id to use inside maintable.test.js file.
     */
      <div data-testid={"MainTableTestId"}>
        {/*
         // * Toolbar and Headers
         */}
        <Toolbar
          data={data}
          title={title}
          columns={columns}
          allColumns={allColumns}
          globalFilter={globalFilter}
          refreshTable={refreshTable}
          toolbarActions={toolbarActions}
          setGlobalFilter={setGlobalFilter}
          selectedFlatRows={selectedFlatRows}
          getToggleHideAllColumnsProps={getToggleHideAllColumnsProps}
        />
        <br />
        {/* // * React Table UI */}
        <Styles>
          <div
            {...getTableProps()}
            className="table sticky"
            style={{ width: "100%", height: height }}
          >
            <div className="header">
              {headerGroups.map((headerGroup) => (
                <div {...headerGroup.getHeaderGroupProps()} className="tr">
                  {headerGroup.headers.map((column) => (
                    <div {...column.getHeaderProps()} className="th">
                      {/* // * HEADERS AND SORTING, */}
                      <Box
                        display="flex"
                        flexWrap="nowrap"
                        flexDirection="row"
                        alignContent="center"
                      >
                        <Box flexGrow={1}>{column.render("Header")}</Box>
                        <Box>
                          {/* // * If the column can be grouped, let's add a toggle */}
                          {column.canGroupBy && (
                            <IconButton
                              size="small"
                              {...column.getGroupByToggleProps()}
                              color="primary"
                            >
                              {column.isGrouped ? (
                                <Lens />
                              ) : (
                                <PanoramaFishEye />
                              )}
                            </IconButton>
                          )}
                        </Box>
                        <Box>
                          <IconButton
                            size="small"
                            {...column.getSortByToggleProps()}
                            color="primary"
                          >
                            {column.canSort &&
                              (column.isSorted ? (
                                column.isSortedDesc ? (
                                  <ArrowUpward />
                                ) : (
                                  <ArrowDownward />
                                )
                              ) : (
                                <SortByAlpha />
                              ))}
                          </IconButton>
                        </Box>
                      </Box>
                      {!column.headers && <Divider />}
                      <Box>
                        {/* // * Render the columns filter UI */}
                        {column.canFilter && column.render("Filter")}
                      </Box>
                      {/* // * Use column.getResizerProps to hook up the events correctly */}
                      {column.canResize && (
                        <div
                          {...column.getResizerProps()}
                          className={`resizer ${
                            column.isResizing ? "isResizing" : ""
                          }`}
                        />
                      )}
                    </div>
                  ))}
                </div>
              ))}
            </div>
            <div {...getTableBodyProps()} className="body">
              {page.map((row) => {
                prepareRow(row);
                return (
                  <React.Fragment key={row.getRowProps().key}>
                    <div {...row.getRowProps()} className="tr">
                      {row.cells.map((cell) => {
                        return (
                          <div {...cell.getCellProps()} className="td">
                            <Box
                              display="flex"
                              flexWrap="nowrap"
                              flexDirection="row"
                              alignContent="center"
                              alignItems="center"
                            >
                              {/* // * If it's a grouped cell, add an expander and row count*/}
                              {cell.isGrouped ? (
                                <React.Fragment>
                                  <Box>
                                    <IconButton
                                      size="small"
                                      {...row.getToggleRowExpandedProps()}
                                    >
                                      {row.isExpanded ? (
                                        <ExpandLess />
                                      ) : (
                                        <ExpandMore />
                                      )}
                                    </IconButton>
                                  </Box>
                                  <Box>
                                    <Typography
                                      component="span"
                                      variant="body1"
                                    >
                                      {cell.render("Cell")} (
                                      {row.subRows.length})
                                    </Typography>
                                  </Box>
                                </React.Fragment>
                              ) : cell.isAggregated ? (
                                <Box>
                                  {/* // * If the cell is aggregated, use the Aggregated
                                  // * renderer for cell */}
                                  <Typography component="span" variant="body1">
                                    {cell.render("Aggregated")}
                                  </Typography>
                                </Box>
                              ) : cell.isPlaceholder ? null : (
                                <Box>
                                  <Typography component="span" variant="body1">
                                    {/* // * For cells with repeated values, render null
                                    // * Otherwise, just render the regular cell */}
                                    {cell.render("Cell")}
                                  </Typography>
                                </Box>
                              )}
                            </Box>
                          </div>
                        );
                      })}
                    </div>
                    {/*
                      // * If the row is in an expanded state, render a row with a
                      // * column that fills the entire length of the table.
                    */}
                    {row.isExpanded && renderRowSubComponent && (
                      <div className="tr">
                        <div className="td" colSpan={visibleColumns.length}>
                          {/*
                            // * Inside it, call our renderRowSubComponent function. In reality,
                            // * you could pass whatever you want as props to
                            // * a component like this, including the entire
                            // * table instance. But for this example, we'll just
                            // * pass the row
                          */}
                          {renderRowSubComponent(row.original)}
                        </div>
                      </div>
                    )}
                  </React.Fragment>
                );
              })}
              <div className="tr">
                {loading && (
                  // * Use our custom loading state to show a loading indicator
                  <div className="td" colSpan="10000">
                    <Progress />
                  </div>
                )}
              </div>
            </div>
          </div>
        </Styles>
        {/* // * Pagination:
        // * Pagination can be built however you'd like. 
        // * This is just a Material UI implementation: */}
        <Pagination
          pageOptions={pageOptions}
          handleChange={handleChange}
          pageIndex={pageIndex}
          pageSize={pageSize}
          setPageSize={setPageSize}
          gotoPage={gotoPage}
        />
      </div>
    );
  }
);
// * Type and required properties
MainTableOrganism.propTypes = {
  data: PropTypes.array,
  title: PropTypes.node,
  select: PropTypes.node,
  height: PropTypes.any.isRequired,
  loading: PropTypes.bool.isRequired,
  columns: PropTypes.array.isRequired,
  indexing: PropTypes.bool,
  fetchData: PropTypes.func.isRequired,
  rowActions: PropTypes.func,
  toolbarActions: PropTypes.func,
  controlledPageCount: PropTypes.number,
  renderRowSubComponent: PropTypes.func,
};
// * Default properties
MainTableOrganism.defaultProps = {};

export default MainTableOrganism;
