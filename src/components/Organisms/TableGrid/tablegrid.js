import React from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS
import {
  TableContainer,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Grid,
  Typography,
  IconButton,
  Table,
  Box,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import {
  useTable,
  useSortBy,
  useFilters,
  useExpanded,
  useRowSelect,
} from "react-table";
// Icons
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import SortIcon from "@material-ui/icons/Sort";
// OTHER
import Toolbar from "../ToolBar";
import {
  DefaultFilterAtom,
  fuzzyTextFilterFn,
} from "../../Atoms/DefaultFilter/defaultfilter";
import Pagination from "../Pagination";
import Progress from "../../Atoms/Progress";
import IndeterminateCheckbox from "../../Atoms/Checkbox";
import Client, { extractColumns } from "../../../Utils/Client";

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const TableGridAtom = React.forwardRef((props, ref) => {
  const {
    uri,
    data,
    page,
    pages,
    title,
    select,
    entity,
    toolbar,
    indexing,
    rowactions,
    callstandard,
    globalfilter,
    defaultfilter,
    toolbaractions,
    detailcomponent,
    toolbarmobileactions,
    ...rest
  } = props;

  const [tableState, setTableState] = React.useState({
    uri: uri,
    data: data || null,
    page: page || 1,
    pages: pages || 1,
    title: title,
    select: select,
    entity: entity,
    toolbar: toolbar,
    columns: extractColumns(props.columns),
    indexing: indexing,
    pageSize: 10,
    filterValue: null,
    callStandard: callstandard,
    globalFilter: globalfilter,
    defaultFilter: defaultfilter,
    toolbarActions: toolbaractions,
    columnsToFilter: [],
    originalColumns: props.columns,
    toolbarMobileActions: toolbarmobileactions,
    renderRowSubComponent: detailcomponent,
  });
  React.useEffect(() => {
    console.log("useEffect");
    FetchFunction();
  }, []);
  // If data is empty or page, pageSize, filterValue are Changed
  React.useLayoutEffect(() => {
    FetchFunction();
  }, [tableState.page, tableState.pageSize, tableState.filterValue]);

  // Update columns to filter to the Global Filter
  const setColumnsToFilter = (newColumns) => {
    setTableState({
      ...tableState,
      columnsToFilter: newColumns,
    });
  };

  // Update filter value to the Global Filter
  const setFilterValue = (newFilterValue) => {
    setTableState({ ...tableState, filterValue: newFilterValue });
  };

  // Update pageSize to Pagination
  const setPageSize = (newSize) => {
    setTableState({ ...tableState, pageSize: newSize });
  };

  // Update page number
  const setPage = (newPage) => {
    setTableState({ ...state, page: newPage });
  };

  // Fuction called to update all data displayed
  async function FetchFunction() {
    console.log("entra", tableState);
    // EBS Standar Fetch call (Logic inside EBS-Grid-Lib)
    if (tableState.uri) {
      const data = await Client(
        tableState.uri,
        tableState.entity,
        tableState.columns,
        tableState.callStandard,
        tableState.page,
        tableState.pageSize,
        tableState.columnsToFilter,
        tableState.filterValue,
        tableState.defaultFilter
      );
      setTableState({
        ...tableState,
        data: data.data,
      });
    } else {
      // Fetch handled by Developer (Logic outside EBS-Grid-Lib)
      this.props.fetch(
        tableState.page,
        tableState.pageSize,
        tableState.columnsToFilter,
        tableState.filterValue
      );
    }
  }

  // Special styles for make responsive the Actions column
  const [width, setWidth] = React.useState(() => {
    const nodesByRow = rowactions
      ? rowactions({}, () => {}).props.children.length
      : 0;
    return (typeof nodesByRow === "number" && 30 * nodesByRow) || 15;
  });

  const useStyles = makeStyles({
    container: {
      height: "85vh",
    },
    row: {
      minWidth: 100,
      maxWidth: "100%",
    },
    actions: {
      width: width,
    },
  });

  const classes = useStyles();
  // Save all columns to display (including actions, expandible and selectable if they are necessary)
  let newColumns = [];
  rowactions &&
    newColumns.push({
      // Make an actions cell
      Header: () => "Actions",
      id: "actions", // It needs an ID
      Cell: ({ row }) => (
        // Use Cell to render actions for each row.
        <Box>{rowactions(row.original, refreshGrid)}</Box>
      ),
    });
  tableState.renderRowSubComponent &&
    newColumns.push({
      // Make an expander cell
      Header: () => null, // No header
      id: "expander", // It needs an ID
      Cell: ({ row }) => (
        // Use Cell to render an expander for each row.
        // We can use the getToggleRowExpandedProps prop-getter
        // to build the expander.
        <span {...row.getToggleRowExpandedProps()}>
          <IconButton size="small">
            {row.isExpanded ? (
              <KeyboardArrowUpIcon />
            ) : (
              <KeyboardArrowDownIcon />
            )}
          </IconButton>
        </span>
      ),
    });
  newColumns = newColumns.concat(tableState.originalColumns);

  // Set hidden columns
  const hiddenColumns = [];
  props.columns.map((column) => {
    column.hidden && hiddenColumns.push(column.accessor);
  });
  !select && hiddenColumns.push("selectable");

  function refreshGrid(column, value) {
    FetchFunction(
      uri,
      entity,
      columns,
      callstandard,
      page,
      pageSize,
      column || columnsToFilter,
      value || filterValue,
      defaultFilter
    );
  }

  // Prepare all columns
  const columns = React.useMemo(() => newColumns, [tableState.pageSize]);

  // Default column settings
  const defaultColumn = React.useMemo(
    () => ({
      // Let's set up our default Filter UI
      Filter: (props) => (
        <DefaultFilterAtom uri={uri} refreshGrid={refreshGrid} {...props} />
      ),
    }),
    []
  );

  const filterTypes = React.useMemo(
    () => ({
      // Add a new fuzzyTextFilterFn filter type.
      fuzzyText: fuzzyTextFilterFn,
      // Or, override the default text filter to use
      // "startWith"
      text: (rows, id, filterValue) => {
        return rows.filter((row) => {
          const rowValue = row.values[id];
          return rowValue !== undefined
            ? String(rowValue)
                .toLowerCase()
                .startsWith(String(filterValue).toLowerCase())
            : true;
        });
      },
    }),
    []
  );

  // Let the table remove the filter if the string is empty
  fuzzyTextFilterFn.autoRemove = (val) => !val;

  // Use the state and functions returned from useTable to build the UI
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    allColumns,
    getToggleHideAllColumnsProps,
    visibleColumns,
    selectedFlatRows,
    state: { expanded },
  } =
    data &&
    useTable(
      {
        defaultColumn, // Be sure to pass the defaultColumn option
        columns,
        data: { ...tableState.data },
        filterTypes,
        initialState: {
          hiddenColumns: hiddenColumns,
        },
      },
      useFilters, // useFilters!
      useSortBy,
      useExpanded, // We can useExpanded to track the expanded state
      useRowSelect, // useRowSelect!
      // for sub components too!
      (hooks) => {
        hooks.visibleColumns.push((columns) => [
          // Let's make a column for selection
          {
            id: "selectable",
            // The header can use the table's getToggleAllRowsSelectedProps method
            // to render a checkbox
            Header: ({ getToggleAllRowsSelectedProps }) => {
              switch (select) {
                case "multi":
                  return (
                    <IndeterminateCheckbox
                      indexing={indexing}
                      color="primary"
                      {...getToggleAllRowsSelectedProps()}
                    />
                  );
                case "single":
                  return <div />;
                default:
                  return <div />;
              }
            },
            // The cell can use the individual row's getToggleRowSelectedProps method
            // to the render a checkbox
            Cell: ({ row }) => {
              switch (select) {
                case "multi":
                  return (
                    <IndeterminateCheckbox
                      indexing={indexing}
                      color="primary"
                      index={row.index}
                      {...row.getToggleRowSelectedProps()}
                    />
                  );
                case "single":
                  if (
                    rows.filter((row) => row.isSelected).length < 1 ||
                    row.isSelected
                  ) {
                    return (
                      <IndeterminateCheckbox
                        indexing={indexing}
                        index={row.index}
                        {...row.getToggleRowSelectedProps()}
                      />
                    );
                  } else {
                    return (
                      <IndeterminateCheckbox
                        checked={false}
                        indexing={indexing}
                        index={row.index}
                        readOnly
                        style={row.getToggleRowSelectedProps().style}
                      />
                    );
                  }
                default:
                  return <div />;
              }
            },
          },
          ...columns,
        ]);
      }
    );

  return (
    /* 
     @prop data-testid: Id to use inside tablegrid.test.js file.
     */
    <div data-testid={"TableGridTestId"}>
      {/* TOOLBAR UI */}
      {toolbar && (
        <Toolbar
          {...tableState}
          allColumns={allColumns}
          rowsSelected={selectedFlatRows}
          hiddenColumns={hiddenColumns}
          FetchFunction={FetchFunction}
          setFilterValue={setFilterValue}
          setColumnsToFilter={setColumnsToFilter}
          getToggleHideAllColumnsProps={getToggleHideAllColumnsProps}
        />
      )}
      {/* TABLE UI */}
      {(tableState.data && (
        <TableContainer className={classes.container}>
          <Table
            ref={ref}
            size={select ? "small" : "medium"}
            {...getTableProps()}
            stickyHeader
            aria-label="sticky table"
          >
            <TableHead>
              {headerGroups.map((headerGroup) => (
                <TableRow {...headerGroup.getHeaderGroupProps()}>
                  {/* Set padding to selectable and expandible column, set sorting and default filter for others */}
                  {headerGroup.headers.map((column) =>
                    column.id.includes("expander") ||
                    column.id.includes("selectable") ? (
                      <TableCell
                        {...column.getHeaderProps()}
                        padding="checkbox"
                      >
                        {/*Set pagind for checkbox and expandible column*/}
                        <Typography variant={"subtitle1"}>
                          {column.render("Header")}
                        </Typography>
                      </TableCell>
                    ) : (
                      (column.id.includes("actions") && ( //
                        <TableCell
                          {...column.getHeaderProps()}
                          className={classes.actions}
                        >
                          <Typography variant={"subtitle1"}>
                            {column.render("Header")}
                          </Typography>
                        </TableCell>
                      )) || (
                        <TableCell
                          {...column.getHeaderProps()}
                          className={classes.row}
                        >
                          <Grid
                            container
                            direction="row"
                            justify="space-between"
                            alignItems="center"
                          >
                            <Grid item xs={11} sm={11} md={11} lg={11} xl={11}>
                              <Typography variant={"subtitle1"}>
                                {column.render("Header")}
                              </Typography>
                            </Grid>
                            <Grid
                              item
                              xs={1}
                              sm={1}
                              md={1}
                              lg={1}
                              xl={1}
                              {...column.getHeaderProps(
                                column.getSortByToggleProps()
                              )}
                            >
                              {column.canSort ? (
                                column.isSorted ? (
                                  column.isSortedDesc ? (
                                    <KeyboardArrowDownIcon />
                                  ) : (
                                    <ExpandLessIcon />
                                  )
                                ) : (
                                  <SortIcon />
                                )
                              ) : null}
                            </Grid>
                            <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
                              {column.canFilter
                                ? column.render("Filter")
                                : null}
                            </Grid>
                          </Grid>
                        </TableCell>
                      )
                    )
                  )}
                </TableRow>
              ))}
            </TableHead>
            <TableBody {...getTableBodyProps()}>
              {rows.map((row, key) => {
                prepareRow(row);
                return (
                  <React.Fragment key={key}>
                    <TableRow {...row.getRowProps()}>
                      {row.cells.map((cell) => {
                        return (
                          <TableCell {...cell.getCellProps()}>
                            {cell.render("Cell")}
                          </TableCell>
                        );
                      })}
                    </TableRow>
                    {/*
                    If the row is in an expanded state, render a row with a
                    column that fills the entire length of the table.
                  */}
                    {row.isExpanded && (
                      <TableRow>
                        <TableCell colSpan={visibleColumns.length}>
                          {/*
                          Inside it, call our renderRowSubComponent function. In reality,
                          you could pass whatever you want as props to
                          a component like this, including the entire
                          table instance. But for this example, we'll just
                          pass the row
                        */}
                          {tableState.renderRowSubComponent(row)}
                        </TableCell>
                      </TableRow>
                    )}
                  </React.Fragment>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
      )) || <Progress />}
      {/* PAGINATION UI */}
      <Pagination {...tableState} setPage={setPage} setPageSize={setPageSize} />
    </div>
  );
});
// Type and required properties
TableGridAtom.propTypes = {};
// Default properties
TableGridAtom.defaultProps = {};

export default TableGridAtom;
