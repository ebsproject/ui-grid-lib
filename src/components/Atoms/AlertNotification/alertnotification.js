import React from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS
import MuiAlert from "@material-ui/lab/Alert";
import { Snackbar } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
}));

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const AlertNotificationAtom = React.forwardRef(
  ({ open, handleClose, message }, ref) => {
    const classes = useStyles();
    return (
      /* 
     @prop data-testid: Id to use inside alertnotification.test.js file.
     */
      <div
        data-testid={"AlertNotificationTestId"}
        ref={ref}
        className={classes.root}
      >
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={open}
          autoHideDuration={6000}
          onClose={handleClose}
        >
          <Alert onClose={handleClose} severity="error">
            {message}
          </Alert>
        </Snackbar>
      </div>
    );
  }
);
// Type and required properties
AlertNotificationAtom.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  message: PropTypes.string,
};
// Default properties
AlertNotificationAtom.defaultProps = {
  open: false,
};

export default AlertNotificationAtom;
