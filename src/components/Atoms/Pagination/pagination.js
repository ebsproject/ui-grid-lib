import React from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS
import { Box, MenuItem, TextField, Typography } from "@material-ui/core";
import { Pagination } from "@material-ui/lab";

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const PaginationAtom = React.forwardRef((props, ref) => {
  const { pageOptions, handleChange, pageIndex,pageSize, setPageSize, gotoPage } = props;

  return (
    /* 
     @prop data-testid: Id to use inside pagination.test.js file.
     */
    <div data-testid={"PaginationTestId"} ref={ref}>
      <Box
        display="flex"
        flexWrap="nowrap"
        flexDirection="row"
        alignItems="center"
      >
        <Box p={1}>
          <Typography component="span" variant="body1">
            <Pagination
              boundaryCount={1}
              color="secondary"
              count={pageOptions.length}
              defaultPage={1}
              onChange={handleChange}
              page={pageIndex + 1}
              shape="round"
              showFirstButton
              showLastButton
              size="medium"
            />
          </Typography>
        </Box>
        <Box p={1}>
          <Typography component="span" variant="body1">
            Page
          </Typography>
        </Box>
        <Box p={1}>
          <strong>
            <Typography component="span" variant="body1">
              {pageIndex + 1} of {pageOptions.length}
            </Typography>
          </strong>{" "}
        </Box>
        <Box p={1}>
          <Typography component="span" variant="body1">
            | Go to page:{" "}
          </Typography>
        </Box>
        <Box p={1}>
          <TextField
            component="span"
            size="small"
            variant="outlined"
            type="number"
            color="secondary"
            defaultValue={pageIndex + 1}
            onChange={(e) => {
              const page = e.target.value ? Number(e.target.value) - 1 : 0;
              gotoPage(page);
            }}
            style={{ width: "70px" }}
          />
        </Box>
        <Box p={1}>
          <TextField
            size="small"
            id="select"
            select
            color="secondary"
            variant="outlined"
            value={pageSize}
            onChange={(e) => {
              setPageSize(Number(e.target.value));
            }}
          >
            {[10, 50, 75, 100].map((pageSize) => (
              <MenuItem key={pageSize} value={pageSize}>
                <Typography component="span" variant="body1">
                  Show {pageSize}
                </Typography>
              </MenuItem>
            ))}
          </TextField>
        </Box>
      </Box>
    </div>
  );
});
// Type and required properties
PaginationAtom.propTypes = {
  pageOptions: PropTypes.array.isRequired,
  handleChange: PropTypes.func.isRequired,
  pageIndex: PropTypes.number.isRequired,
  pageSize: PropTypes.number.isRequired,
  setPageSize: PropTypes.func.isRequired,
  gotoPage: PropTypes.func.isRequired,
};
// Default properties
PaginationAtom.defaultProps = {};

export default PaginationAtom;
