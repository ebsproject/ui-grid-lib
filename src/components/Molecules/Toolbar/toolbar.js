import React from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS AND ATOMS TO USE
import { Box, IconButton } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import GlobalFilter from "../../Atoms/GlobalFilter";
import UserPreferences from "../UserPreferences";
import CSVExport from "../../../functions/csv";
import { GetApp } from "@material-ui/icons";
import { rest } from "lodash";

const useStyles = makeStyles((theme) => ({
  button: {
    "&:hover":{
      backgroundColor: theme.palette.secondary.main,
    }, 
    color: theme.palette.common.white,
    backgroundColor: theme.palette.primary.main,
    marginLeft: theme.spacing(1),
    borderRadius: 4,
  },
}));
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ToolbarMolecule = React.forwardRef(
  (
    {
      data,
      title,
      columns,
      refreshTable,
      globalFilter,
      toolbarActions,
      setGlobalFilter,
      selectedFlatRows,
      ...rest,
    },
    ref
  ) => {
    const classes = useStyles();

    const csvExport = () => {
      let headers = {};
      let formatData = [];
      // Format Headers
      columns.map((column) => {
        if (!column.hidden) {
          headers[column.accessor] = column.Header;
        }
      });
      // Format Data
      data.map((item) => {
        let itemFormatted = {};
        columns.map((column) => {
          if (!column.hidden) {
            itemFormatted[column.accessor] = item[column.accessor];
          }
        });
        formatData.push(itemFormatted);
      });
      // Export Data to CSV
      CSVExport(headers, formatData, `${title.replace(" ", "")}CSV`);
    };

    return (
      /* 
     @prop data-testid: Id to use inside toolbar.test.js file.
     */
      <div ref={ref} data-testid={"ToolbarTestId"}>
        <Box display="flex" alignItems="center">
          <Box flexGrow={1}>{title}</Box>
          <Box>
            <GlobalFilter
              globalFilter={globalFilter}
              setGlobalFilter={setGlobalFilter}
            />
          </Box>
          <Box>
            {toolbarActions &&
              toolbarActions(
                selectedFlatRows.map((d) => d.original),
                refreshTable
              )}
          </Box>
          <Box>
            <IconButton
              className={classes.button}
              onClick={csvExport}
              color="secondary"
              aria-label="download file"
            >
              <GetApp />
            </IconButton>
          </Box>
          <Box>
            <UserPreferences {...rest}/>
          </Box>
        </Box>
      </div>
    );
  }
);
// Type and required properties
ToolbarMolecule.propTypes = {
  title: PropTypes.node,
  refreshTable: PropTypes.func,
  globalFilter: PropTypes.any,
  toolbarActions: PropTypes.func,
  setGlobalFilter: PropTypes.func.isRequired,
  selectedFlatRows: PropTypes.array.isRequired,
};
// Default properties
ToolbarMolecule.defaultProps = {};

export default ToolbarMolecule;
