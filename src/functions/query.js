import { gql } from "@apollo/client";
export const buildQuery = (entity, apiContent) => {
  return gql`
    query find${entity}List($page: PageInput, $sort: SortInput, $filters: [FilterInput], $disjunctionFilters: Boolean){
        find${entity}List (page:$page, sort:$sort, filters:$filters, disjunctionFilters: $disjunctionFilters){
            totalPages,
            content {
                ${apiContent.map((item) => {
                  const levels = item.accessor.split(".");
                  // Setting the second level up
                  if (levels.length > 1) {
                    return `${levels[0]}{
                            ${levels[1]}
                        }`;
                  } else {
                    return `${levels[0]}`;
                  }
                })}
            }
        }
    }`;
};
